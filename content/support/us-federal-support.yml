---
title: US Federal Support
description: US Federal Support
side_menu:
  links:
  - text: US Federal Support
    href: "#us-federal-support"
    children:
    - text: Limitations
      href: "#limitations"
    - text: Hours of operation
      href: "#hours-of-operation"
    - text: US Federal Emergency support
      href: "#us-federal-emergency-support"
    - text: CCs are disabled
      href: "#ccs-are-disabled"
    - text: Scheduling call without access to Calendly
      href: "#scheduling-call-without-access-to-calendly"
components:
- name: call-to-action
  data:
    title: US Federal Support
    centered_by_default: true
    hide_title_image: true
- name: copy
  data:
    block:
    - hide_horizontal_rule: true
      no_margin_bottom: true
      text: |
        US Federal Support is built for companies and organizations that require that only US Citizens have access to the data contained within their support issues. The unique requirements of US Federal Support result in the following specific policies:

        ### Limitations {#limitations}

        To be eligible to utilize this instance, your account manager must approve and you must meet one of the following criteria:

        1. Be using a Premium or Ultimate License
        1. Be using a Starter license with 2000 or more license seats

        For more information about utilizing this method of support, please contact your Account Manager.

        ### Hours of operation {#hours-of-operation}

        Due to the requirement that support be provided only by US citizens, US Federal Support hours of operation are limited to:

        * Monday through Friday, 0500-1700 Pacific Time

        ### US Federal Emergency support {#us-federal-emergency-support}

        The US Federal instance offers emergency support. However, this is limited to the hours of 0500 to 1700 Pacific Time 7 days a week.

        Emergencies can be filed either via the email address you should have received from your Account Manager or via the [Emergency support request form](https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001421112).

        ### CCs are disabled {#ccs-are-disabled}
        To help ensure that non-US citizens are not inadvertently included in a support interaction, the "CC" feature is disabled. As a result, support personnel will be unable to add additional contacts to support tickets.

        By request through your account manager, you may allow certain individuals in your organization the ability to view and respond to any open support tickets through the US Federal Support Portal.

        ### Scheduling call without access to Calendly {#scheduling-call-without-access-to-calendly}
        If a Support Engineer [determines that a call is beneficial](https://about.gitlab.com/support/#phone-and-video-call-support){data-ga-name="phone and video call support" data-ga-location="body"} but your organization's network rules prevent you from accessing Calendly to choose a time-slot, please propose three future dates/times in the ticket with appropriate lead up time. The assigned Support Engineer will attempt to schedule the call during the requested timeframe and share the invitation and meeting link in the ticket.
